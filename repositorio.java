/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhofinal;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author Utilizador
 */
public class Repositorio {
    private Map<Integer, Gestor> gestores;
    private Map<Integer, Secretario> secretarios
    private Map<Integer, Excurcao> excurcoes;
    
    private static Repositorio repositorio = null;
    
    private Repositorio(){
    }
    
    public static Repositorio getInstance(){
        if (repositorio == null){
            repositorio = new Repositorio();
        }
        return repositorio;
    }
    
    
    
    
    public void novaExcurcao(Excurcao excurcao){
        this.excurcoes.put(excurcao.getId(), excurcao);
    }

    
    
    public static void serializar(String filename) {
        // Serializar um objeto para ficheiro
        try(FileOutputStream fileOut = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut))
        {
            out.writeObject(repositorio);
            System.out.println("Serialized data is saved in " + filename);
        } catch (IOException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
    }
   
    public static void desserializar(String filename) {
        try {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            repositorio = (Repositorio) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException ex) {
            System.out.println("Erro: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Cliente class not found. " + ex.getMessage());
        }
    }
}
